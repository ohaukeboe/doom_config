(setq user-full-name "Oskar Haukebøe"
      user-mail-address "ohaukeboe@pm.me")

(load! "bindings")

(setq doom-font (font-spec :family "RobotoMono Nerd Font" :size 13 :weight 'semi-light)
     doom-variable-pitch-font (font-spec :family "Roboto" :size 18))

;;(setq doom-theme 'doom-one)
;;(setq doom-theme 'doom-Iosvkem)
(setq doom-theme 'doom-tomorrow-night)

;; Hei
(custom-set-faces
  '(font-lock-comment-face ((t (:foreground "#7c7d7c")))))

(setq display-line-numbers-type 'relative)

(defun eide-smart-tab-jump-out-or-indent (&optional arg)
  "Smart tab behavior. Jump out quote or brackets, or indent."
  (interactive "P")
  (if (-contains? (list "\"" "'" ")" "}" ";" "|" ">" "]" "$" ) (make-string 1 (char-after)))
      (forward-char 1)
    (indent-for-tab-command arg)))

(global-set-key [remap indent-for-tab-command]
                'eide-smart-tab-jump-out-or-indent)

(after! treemacs
  (setq!
   treemacs-follow-mode t
   treemacs-tag-follow-delay 0
   treemacs-width 28))

(after! highlight-indent-guides
  (setq!
   highlight-indent-guides-method 'bitmap
   highlight-indent-guides-auto-character-face-perc 50;;120
   highlight-indent-guides-responsive "stack"))

(when (and (eq system-type 'gnu/linux)
           (string-match
            "Linux.*Microsoft.*Linux"
            (shell-command-to-string "uname -a")))
  (setq
   browse-url-generic-program  "/mnt/c/Windows/System32/cmd.exe"
   browse-url-generic-args     '("/c" "start")
   browse-url-browser-function #'browse-url-generic))

(after! org
  (setq org-directory "~/Nextcloud/org_notes/")
  (setq org-attach-id-dir "~/Nextcloud/org_notes/.attach/")

  (when (string-match "Linux Oskars-flow-x13*" (shell-command-to-string "uname -a"))
     (setq org-directory "/mnt/c/Users/Oskar/Nextcloud/org_notes/"))

  (when (string-match "Linux oskar-rogflowx13*" (shell-command-to-string "uname -a"))
     (setq org-directory "~/Nextcloud/org_notes/")
     (setq org-attach-id-dir "~/Nextcloud/org_notes/.attach/")))

(add-hook! org-mode
  (setq display-line-numbers 'nil))

(after! org
  (setq! org-pretty-entities t)
  (+org-pretty-mode t))

(add-hook! org-mode
           :append
           #'org-fragtog-mode)

(after! org-appear
 (setq! org-appear-inside-latex t)
 (setq! org-appear-autosubmarkers t))

(use-package! org-modern-mode
  :hook org-mode)

(after! org-modern
  (setq! org-modern-priority nil
           org-modern-table nil))

(after! org
  (setq!
        org-format-latex-options (plist-put org-format-latex-options :scale 1.3) ;; Set scale of preview images
        org-export-with-tags nil
        org-startup-with-inline-images t
        org-startup-with-latex-preview t))

(add-hook! org-mode #'toc-org-mode)

(defun individual-visibility-source-blocks ()
  "Fold some blocks in the current buffer."
  (interactive)
  (org-show-block-all)
  (org-block-map
   (lambda ()
     (let ((case-fold-search t))
       (when (and
              (save-excursion
                (beginning-of-line 1)
                (looking-at org-block-regexp))
              (cl-assoc
               ':hidden
               (cl-third
                (org-babel-get-src-block-info))))
         (org-hide-block-toggle))))))

(add-hook
 'org-mode-hook
 (function individual-visibility-source-blocks))

(after! org-noter
  (setq! org-noter-separate-notes-from-heading t)
  (setq! org-noter-highlight-selected-text t))

(after! org-noter
  (setq! org-noter-notes-search-path '("~/Nextcloud/org_notes/" "~/Nextcloud/org_notes/roam/bibliography/"))
  (setq! org-noter-doc-property-in-notes t))

(after! org-roam
  (setq org-roam-directory (file-truename "~/Nextcloud/org_notes/roam")))

(use-package! websocket
    :after org-roam)

(use-package! org-roam-ui
    :after org-roam;; or :after org
;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
;;         a hookable mode anymore, you're advised to pick something yourself
;;         if you don't care about startup time, use
;;  :hook (after-init . org-roam-ui-mode)
    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
          org-roam-ui-open-on-start t
          completion-ignore-case t)
    (map!
        :leader :n "n r u" #'org-roam-ui-mode))

(after! org-roam
  (setq! org-roam-capture-templates
            '(("d" "default" plain "%?"
                 :target
                      (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
                 :unnarrowed t
                ("r" "reference" plain "%?"
                     :if-new
                     (file+head "references/%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+filetags: %^{filetags}\n")
                     :immediate-finish t
                     :unnarrowed t)))))
               ;; ("n" "bibliography reference" plain
               ;;       (file "~/.doom.d/templates/roam/noter.org")
               ;;       :target
               ;;       (file+head "bibliography/${citekey}.org" "#+title: ${title}\n"))))

;;(add-hook 'org-mode-hook #'org-inline-pdf-preview)
(use-package! org-inline-pdf
  :hook (org-mode . org-inline-pdf-mode))

(add-hook! org-mode
  (auto-fill-mode 1))

(setq! org-cite-global-bibliography '("~/Nextcloud/.org/references.bib"
                                      "~/Nextcloud/.org/bibliography/zotero.bib"
                                      "~/Nextcloud/.org/bibliography/uni/IN2000 gang.bib"
                                      "~/Nextcloud/.org/bibliography/uni/IN2120_gang-midterm.bib")
        org-cite-csl-styles-dir "~/Zotero/styles"
        org-cite-csl--fallback-style-file "~/Zotero/styles/ieee.csl")

(setq! citar-bibliography org-cite-global-bibliography
        citar-library-paths '("~/Nextcloud/.org/library/")
        citar-notes-paths '("~/Nextcloud/org_notes/roam/bibliography/"))
        ;;citar-citeproc-csl-styles-dir "~/Zotero/styles/"
        ;;citar-citeproc-csl-style "vancouver.csl"
        ;;citar-file-parser-functions '(citar-file-parser-default citar-file-parser-triplet))

(setq! citar-symbols
      `((file ,(all-the-icons-faicon "file-o" :face 'all-the-icons-green :v-adjust -0.1) . " ")
        (note ,(all-the-icons-material "speaker_notes" :face 'all-the-icons-blue :v-adjust -0.3) . " ")
        (link ,(all-the-icons-octicon "link" :face 'all-the-icons-orange :v-adjust 0.01) . " ")))
(setq! citar-symbol-separator "  ")

(after! citar-org-roam
  (setq! citar-org-roam-subdir "bibliography"))

(after! citar-org-roam
  (defun citar-org-roam--create-capture-note (citekey entry)
      "Open or create org-roam node for CITEKEY and ENTRY."
     ;; adapted from https://jethrokuan.github.io/org-roam-guide/#orgc48eb0d
      (let ((title (citar-format--entry
                     citar-org-roam-note-title-template entry)))
       (org-roam-capture-
        :templates
        '(("r" "reference" plain "%?" :if-new
           (file+head
            "%(concat
   (when citar-org-roam-subdir (concat citar-org-roam-subdir \"/\")) \"${citekey}.org\")"
            "#+title: ${title}\n")
           :immediate-finish t
           :unnarrowed t)
          ("n" "noter" plain "%?" :if-new
           (file+head
            "%(concat
   (when citar-org-roam-subdir (concat citar-org-roam-subdir \"/\")) \"${citekey}.org\")"
            "#+title: ${title}\n\n* Noter\n:PROPERTIES:\n:NOTER_DOCUMENT: ../../../.org/library/${citekey}.pdf\n:END:\n")
           :immediate-finish t
           :unnarrowed t))
        :info (list :citekey citekey)
        :node (org-roam-node-create :title title)
        :props '(:finalize find-file))
       (org-roam-ref-add (concat "@" citekey)))))

(map! :leader
      (:prefix-map ("n b" . "citar")
        :desc "Bibliographic notes" "n" #'citar-open-notes
        :desc "Open source" "o" #'citar-open))

(use-package! org-roam-bibtex
  :after org-roam)

(after! org
  :config
  ;;(org-export-with-tags 0)
  (add-to-list 'org-latex-packages-alist '("" "minted" t))
  (add-to-list 'org-latex-packages-alist '("" "xcolor" t))
  (add-to-list 'org-latex-packages-alist '("" "dsfont" t))
  (add-to-list 'org-latex-packages-alist '("" "circuitikz" t))
  (add-to-list 'org-latex-packages-alist '("AUTO" "babel" t ("pdflatex")))
  (setq org-latex-listings 'minted)

  (setq org-latex-minted-options
     '(("frame" "lines")
       ("framesep" "2mm")
       ("fontsize" "\\footnotesize")
       ("baselinestretch" "1.2")))

  (setq org-latex-pdf-process
        '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
                "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
                "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
  (setq org-src-fontify-natively t))

(use-package! poporg
  :hook prog-mode-hook
  :bind (("<space> m o" . poporg-dwim)))

(map! :map org-cdlatex-mode-map
      "`" nil
      :i "TAB" #'cdlatex-tab)

(map! :map cdlatex-mode-map
      "`" nil
      :i "TAB" #'cdlatex-tab)

(set-email-account! "proton"
  '((mu4e-sent-folder       . "/proton/Sent")
    (mu4e-drafts-folder     . "/proton/Drafts")
    (mu4e-trash-folder      . "/proton/Trash")
    (mu4e-refile-folder     . "/proton/All Mail")
    (smtpmail-smtp-user     . "ohaukeboe@pm.me")
    (mu4e-compose-signature . "---\nRegards\nOskar"))
  t)

(after! mu4e
  (setq! mu4e-split-view 'vertical))

(use-package! speed-type
  :commands (speed-type-text speed-type-region speed-type-buffer))

(map!
 :leader
 :desc "Calc" "o c" 'calc-dispatch)

(use-package! wakatime-mode
  :init (global-wakatime-mode)
  :config
  (setq wakatime-disable-on-error t)
  (setq wakatime-cli-path "~/.wakatime/wakatime-cli"))

(use-package! copilot
  ;; :hook (prog-mode . copilot-mode)
  :after prog-mode
  :config
  (global-copilot-mode)
  (map! :leader
        :desc "Github Copilot" :n "t a" #'global-copilot-mode)
  :bind (("C-TAB" . 'copilot-accept-completion-by-word)
         ("C-<tab>" . 'copilot-accept-completion-by-word)
         :map copilot-completion-map
         ("<tab>" . 'copilot-accept-completion)
         ("TAB" . 'copilot-accept-completion)))

(defvar openai-key-memo nil "Memoized OpenAI key")

(use-package! chatgpt-shell
  :commands (chatgpt-shell)
  :custom
  ((chatgpt-shell-openai-key
    (lambda ()
      (with-memoization openai-key-memo (auth-source-pass-get 'secret "openai-key")))))
  :init
  (map! :leader
        :prefix "o"
        :desc "ChatGPT" "g" #'chatgpt-shell))

(use-package! dall-e-shell
  :commands (dall-e-shell)
  :custom
  ((dall-e-shell-openai-key
    (lambda ()
      (with-memoization openai-key-memo (auth-source-pass-get 'secret "openai-key")))))
  :config
  (setq! dall-e-shell-image-output-directory "~/Nextcloud/pictures")
  :init
  (map! :leader
        :prefix "o"
        :desc "Dall-e" "s" #'dall-e-shell))

(use-package! ob-chatgpt-shell
  :after org
  :init 'ob-chatgpt-shell-setup)

(after! ispell
  (ispell-set-spellchecker-params)
  (ispell-hunspell-add-multi-dic "en_US,nb_NO")
  (setq ispell-dictionary "en_US,nb_NO"))

(after! preview
  (set-default 'preview-scale-function 0.6))
;;(after! preview
  ;;set-default #'preview-scale-from-face 0.6)

(after! latex-mode
  (setq +latex-viewers '(pdf-tools evince zathura okular skim sumatrapdf)))

(after! pdf-tools
  (pdf-loader-install))

(global-auto-revert-mode)

(add-hook! 'help-mode-hook
  #'+word-wrap-mode)

(vimish-fold-global-mode 1)

(after! vterm
        (setq vterm-environment '("theme_powerline_fonts=no")))

(use-package! doc-show-inline
  :commands (doc-show-inline-mode)
  ;;(define-key c-mode-map (kbd "C-;") 'doc-show-inline-mode)
  ;;(define-key c++-mode-map (kbd "C-;") 'doc-show-inline-mode)

  :hook ((c-mode . doc-show-inline-mode)
         (doc-show-inline-mode . doc-show-inline-buffer)))
            ;; (c++-mode . doc-show-inline-mode))))

(map!
    :leader :n "t d" #'doc-show-inline-mode)

(after! lsp-mode
  (setq! lsp-enable-folding t))

(map! :map prog-mode-map
      :leader
      :desc "Make Run" :n "c m" #'+make/run)

(after! java
  (setq +format-with-lsp nil))

(after! lsp-clangd
  (setq lsp-clients-clangd-args
        '("-j=3"
          "--background-index"
          "--clang-tidy"
          "--completion-style=detailed"
          "--header-insertion=iwyu"
          "--header-insertion-decorators"
          "--fallback-style=linux"))
  (set-lsp-priority! 'clangd 2))

;;(setq! plantuml-jar-path "/usr/share/java/plantuml/plantuml.jar")

;;(use-package! maude-mode)

(after! rust-mode
  (require 'dap-cpptools))
