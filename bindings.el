(map! :map org-noter-doc-mode-map
      :desc "Insert note at page"
      :nv "I" #'org-noter-insert-note)

(map! :map latex-mode-map
      :leader
      :desc "Insert citation"
      :nv "m @" #'citar-insert-citation)
