;;(package! org-appear)
(package! org-fragtog)

(package! org-modern)

;;(unpin! org)

;;(unpin! org-roam)
(package! org-roam-ui)

(package! org-inline-pdf)

(package! org-roam-bibtex
  :recipe (:host github :repo "org-roam/org-roam-bibtex"))

;; When using org-roam via the `+roam` flag
;;(unpin! org-roam)

;; When using bibtex-completion via the `biblio` module
;;(unpin! bibtex-completion helm-bibtex ivy-bibtex)

(package! poporg)

(package! speed-type)

(package! wakatime-mode)

(package! copilot
  :recipe (:host github :repo "zerolfx/copilot.el" :files ("*.el" "dist")))

(package! chatgpt-shell)
(package! ob-chatgpt-shell)
(package! dall-e-shell)
(package! ob-dall-e-shell)

;;(unpin! pdf-tools)

(package! doc-show-inline)

(package! arduino-mode)

(package! maude-mode)
